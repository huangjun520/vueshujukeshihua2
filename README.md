# 简单易用的数据可视化大屏工具springboot+vue

#### 官网
1.  情天数据可视化 [www.51qingtian.com](https://www.51qingtian.com)
2.  测试账号：lisi 密码：123

#### 介绍
基于SpringBoot+Vue3+mysql开发，支持多种数据源：excel、api接口、mysql、oracle、SqlServer等多种类型的数据源，支持数据模型转换，图形化编辑界面：拖拽即可完成样式和数据配置，无需编程就能轻松搭建数据大屏。私有化部署：使用私有化部署的方式，保障贵公司的数据安全，数据大屏支持加密发布

#### 软件架构
情天可视化平台是基于SpringBoot+Vue3+mysql构建的数据可视化开发平台，数据库方面，基于springboot开发的mysql中间件，理论上可以无限扩容mysql节点，完全不用担心数据容量问题；每增加一个mysql节点，只需要在数据库添加一条节点记录即可。


#### 系统特点

1.  支持多种数据源
> 支持excel、api接口、mysql、oracle、SqlServer等多种类型的数据源
2.  支持数据模型转换
> 可以根据具体的使用场景，将原始的数据源数据抽取出一个个独立的数据模型，将数据归类
3.  图形化编辑界面
> 拖拽即可完成样式和数据配置，无需编程就能轻松搭建数据大屏。
4.  私有化部署
> 使用私有化部署的方式，保障贵公司的数据安全，数据大屏支持加密发布

#### 界面展示

1.  大屏编辑界面
![Image text](https://gitee.com/huangjun520/love-day-data-visualization/raw/master/51qingtian.com/images/1.png)(https://www.51qingtian.com)
2.  可视化大屏
![Image text](https://gitee.com/huangjun520/love-day-data-visualization/raw/master/51qingtian.com/images/2.png)(https://www.51qingtian.com)
3.  数据模型
![Image text](https://gitee.com/huangjun520/love-day-data-visualization/raw/master/51qingtian.com/images/3.png)(https://www.51qingtian.com)
4.  数据源
![Image text](https://gitee.com/huangjun520/love-day-data-visualization/raw/master/51qingtian.com/images/4.png)(https://www.51qingtian.com)

#### 模板展示

1.  健身数据报告
> ![Image text](https://gitee.com/huangjun520/love-day-data-visualization/raw/master/51qingtian.com/images/m1.png)(https://www.51qingtian.com)
2.  智慧园区数据统计中心
> ![Image text](https://gitee.com/huangjun520/love-day-data-visualization/raw/master/51qingtian.com/images/m2.png)(https://www.51qingtian.com)
3.  交通安全主题
> ![Image text](https://gitee.com/huangjun520/love-day-data-visualization/raw/master/51qingtian.com/images/m3.png)(https://www.51qingtian.com)
4.  财务数据大屏
> ![Image text](https://gitee.com/huangjun520/love-day-data-visualization/raw/master/51qingtian.com/images/m4.png)(https://www.51qingtian.com)
5.  智慧医疗大数据可视化大屏
> ![Image text](https://gitee.com/huangjun520/love-day-data-visualization/raw/master/51qingtian.com/images/m5.png)(https://www.51qingtian.com)


#### 官网
1.  情天数据可视化 [www.51qingtian.com](https://www.51qingtian.com)


